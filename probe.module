<?php
/**
 * Implements hook_menu().
 */
function probe_menu() {
  $items = array();

  $items['admin/config/probe'] = array(
    'title' => 'Probe',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'file' => '/system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $items['admin/config/probe/probe'] = array(
    'title' => 'Probe',
    'description' => 'Configure the Probe client.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('probe_config'),
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/config/probe/self'] = array(
    'title' => 'Probe self',
    'description' => 'Go Probe yourself.',
    'page callback' => 'probe_self',
    'access arguments' => array('administer site configuration'),
  );

  return $items;
}

/**
 * Page callback for admin/config/probe/self.
 */
function probe_self() {
  $info = probe(array('cron_last'));

  if (function_exists('dpm')) {
    dpm($info);
    return '';
  }

  return '<pre>' . var_export($info, TRUE) . '</pre>';
}

/**
 * Implements hook_xmlrpc().
 */
function probe_xmlrpc() {
  $methods[] =  array(
    'probe', // First argument is the method name.
    'probe', // Callback to execute when this method is requested.
    array( // An array defines the types of output and input values for this method.
      'array', // The first value is the return type, an array in this case.
      'array', // An array with requested variables.
    ),
    t('@TODO'),
  );

  return $methods;
}

/**
 * XMLRPC callback to retrieve info about the used modules and themes.
 */
function probe($variables) {
  if (($access = _probe_xmlrpc_access()) !== TRUE) {
    return $access;
  }

  variable_set('probe_last_probed', REQUEST_TIME);
  watchdog('probe', 'Just got probed by @ip', array('@ip' => ip_address()), WATCHDOG_INFO);

  $modules = array();
  $metadata = array(
    'drupal_version' => VERSION,
    'drupal_root' => DRUPAL_ROOT,
    'base_url' => $GLOBALS['base_url'],
    'num_users' => _probe_users(),
    'num_users_roles' => _probe_users_roles(),
    'num_nodes_type' => _probe_nodes_types(),
    'database_updates' => array(),
    'overridden_features' => array(),
    'install_profile' => variable_get('install_profile', ''),
    'domains' => array(),
    'requirement_issues' => _probe_requirements_status(),
  );

  // Load all install files.
  require_once DRUPAL_ROOT . '/includes/install.inc';
  drupal_load_updates();

  foreach (module_list() as $module) {
    // Check installed modules.
    $path = drupal_get_path('module', $module);
    $info = $path . '/' . $module . '.info';
    if (file_exists($info)) {
      $modules[$module] = array(
        'info' => drupal_parse_info_file($info),
        'path' => DRUPAL_ROOT . '/' . $path,
      );
      $modules[$module]['info']['_info_file_ctime'] = filectime($info);
    }

    // Check if all modules have ran their updates.
    $updates = drupal_get_schema_versions($module);
    if ($updates !== FALSE) {
      $default = drupal_get_installed_schema_version($module);
      if (max($updates) > $default) {
        $metadata['database_updates'][] = $module;
      }
    }
  }

  // Check if all features have reverted their features.
  if (module_exists('features')) {
    module_load_include('export.inc', 'features');
    $feature_states = features_get_component_states();
    $metadata['overridden_features'] = array_filter(array_map('array_keys', array_map('array_filter', $feature_states)));
  }

  $themes = array();
  foreach (list_themes() as $theme) {
    if ($theme->status || variable_get('admin_theme', '') == $theme->name) {
      $themes[$theme->name] = array(
        'info' => $theme->info,
        'path' => DRUPAL_ROOT . '/' . drupal_get_path('theme', $theme->name),
      );
    }
  }

  $vars = array();
  $variables_filtered = array_intersect($variables, variable_get('probe_variables_whitelist', array('cron_last')));
  foreach ($variables_filtered as $key) {
    $vars[$key] = variable_get($key, FALSE);
  }

  $root = user_load(1);
  $users = array(
    'root' => array(
      'name' => $root->name,
      'mail' => $root->mail,
    ),
  );

  $libraries = array();
  if (module_exists('libraries')) {
    foreach (libraries_info() as $name => $library) {
      $library = libraries_detect($name);
      $libraries[$name] = array(
        'info' => $library,
        'path' => DRUPAL_ROOT . '/' . $library['library path'],
      );
    }
  }

  $apis = array();
  foreach (module_invoke_all('probe_api_info') as $name => $api) {
    $apis[$name] = array(
      'info' => $api,
      'path' => drupal_get_path('module', $api['implementing_module']),
    );
  }

  // Number of logs per day.
  if (module_exists('dblog')) {
    $log = db_query('SELECT COUNT(1) logs, MIN(timestamp) min, MAX(timestamp) max FROM {watchdog}')->fetch();
    $days = $log->max > $log->min ? max(1, ($log->max - $log->min) / 86400) : 0;
    $metadata['logs'] = $log->logs && $days ? $log->logs / $days : 0;
  }

  // EMA environment.
  if (!module_exists('ezmod_always')) {
    $metadata['ema_env'] = 'no_ema';
  }
  elseif (!($metadata['ema_env'] = variable_get('ezmod_always_environment', ''))) {
    $metadata['ema_env'] = 'unknown';
  }

  if (module_exists('domain')) {
    $metadata['domains'] = domain_list_by_machine_name();
  }

  // Find the correct platform.
  $is_quadrupal = file_exists(DRUPAL_ROOT . '/profiles/quadrupal/quadrupal.profile');
  $platform = $is_quadrupal ? 'quadrupal' : $metadata['install_profile'];

  $data = array(
    'users' => $users,
    'variables' => $vars,
    'platform' => $platform,
    'site_name' => variable_get('site_name', ''),
    'site_mail' => variable_get('site_mail', ''),
    'metadata' => $metadata,
    'modules' => $modules,
    'libraries' => $libraries,
    'themes' => $themes,
    'apis' => $apis,
  );

  drupal_alter('probe_metadata', $data);

  return $data;
}

/**
 * Helper to check for requirements with severity 'REQUIREMENT_ERROR'.
 */
function _probe_requirements_status() {
  module_load_include('inc', 'system', 'system.admin');
  return system_status(TRUE);
}

/**
 * Helper to get the number of users per status.
 */
function _probe_users() {
  // SELECT status, COUNT(1) num FROM users WHERE uid <> 0 GROUP BY status
  $query = db_select('users', 'u')
    ->fields('u', array('status'))
    ->condition('uid', 0, '<>')
    ->groupBy('status');
  $query->addExpression('COUNT(1)', 'num');
  $num_users = $query->execute()->fetchAllKeyed(0, 1);
  return $num_users;
}

/**
 * Helper to get the number of users per role per status.
 */
function _probe_users_roles() {
  // SELECT u.status, r.name, COUNT(1) num FROM users u JOIN users_roles ur ON ur.uid = u.uid JOIN role r ON ur.rid = r.rid GROUP BY r.rid, u.status
  $query = db_select('users', 'u')
    ->fields('u', array('status'))
    ->fields('r', array('rid', 'name'));
  $query->addExpression('COUNT(1)', 'num');
  $query->groupBy('rid');
  $query->groupBy('status');
  $query->join('users_roles', 'ur', 'ur.uid = u.uid');
  $query->join('role', 'r', 'ur.rid = r.rid');
  $num_users = array();
  foreach ($query->execute() as $row) {
    $num_users[$row->name][$row->status] = $row->num;
  }
  return $num_users;
}

/**
 * Helper to get the number of nodes per type per status.
 */
function _probe_nodes_types() {
  $query = db_select('node', 'n')
    ->fields('n', array('type', 'status'));
  $query->addExpression('COUNT(1)', 'num');
  $query->groupBy('type');
  $query->groupBy('status');
  $num_nodes = array();
  foreach ($query->execute() as $row) {
    $num_nodes[$row->type][$row->status] = $row->num;
  }
  return $num_nodes;
}

/**
 * Helper function to determin access to the XMLRPC call.
 */
function _probe_xmlrpc_access() {
  // Check for probe_key.
  $incoming_probe_key = @$_REQUEST['probe_key'];
  $allowed_probe_key = trim(variable_get('probe_key', FALSE));
  if ($incoming_probe_key && $incoming_probe_key === $allowed_probe_key) {
    return TRUE;
  }

  // Check for sender IP whitelist.
  $incoming_ip = ip_address();
  $allowed_ips = array_filter(preg_split('#(\r\n|\r|\n)#', variable_get('probe_xmlrpc_ips', '127.0.0.1')));
  if (in_array($incoming_ip, $allowed_ips)) {
    return TRUE;
  }

  // Access denied, generic message.
  include_once 'includes/xmlrpc.inc';
  return xmlrpc_error(403, t('Access denied for this IP (@ip) and probe key (@probe_key).', array(
    '@ip' => $incoming_ip,
    '@probe_key' => $incoming_probe_key,
  )));
}

/**
 * Menu callback to configure probe settings.
 */
function probe_config() {
  $form = array();

  $form['probe_xmlrpc_ips'] = array(
    '#title' => t('Allowed IP addresses for Probe XMLRPC calls'),
    '#type' => 'textarea',
    '#default_value' => variable_get('probe_xmlrpc_ips', '127.0.0.1'),
    '#description' => t('Put each IP addres on a new line. Wildcards not allowed.'),
  );

  return system_settings_form($form);
}
